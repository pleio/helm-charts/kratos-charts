# kratos-idm

![Version: 0.3.3](https://img.shields.io/badge/Version-0.3.3-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Source Code

* <https://gitlab.com/pleio/helm-charts/kratos-charts>

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://k8s.ory.sh/helm/charts | hydra | 0.52.0 |
| https://k8s.ory.sh/helm/charts | kratos | 0.52.0 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| databaseUrl | string | `""` |  |
| deployment.annotations | object | `{}` |  |
| deployment.automountServiceAccountToken | bool | `false` |  |
| deployment.dnsConfig | object | `{}` | Configure pod dnsConfig. |
| deployment.extraEnv | list | `[]` | Array of extra envs to be passed to the deployment. Kubernetes format is expected - name: FOO   value: BAR |
| deployment.extraVolumeMounts | list | `[]` |  |
| deployment.extraVolumes | list | `[]` | If you want to mount external volume For example, mount a secret containing Certificate root CA to verify database TLS connection. |
| deployment.labels | object | `{}` |  |
| deployment.nodeSelector | object | `{}` | Node labels for pod assignment. |
| deployment.resources | object | `{}` |  |
| deployment.terminationGracePeriodSeconds | int | `60` |  |
| deployment.tolerations | list | `[]` | Configure node tolerations. |
| deployment.topologySpreadConstraints | list | `[]` | Configure pod topologySpreadConstraints. |
| fullnameOverride | string | `""` |  |
| hydra.fullnameOverride | string | `"idm-hydra"` |  |
| hydra.hydra.automigration.enabled | bool | `true` |  |
| hydra.hydra.config.serve.tls.allow_termination_from[0] | string | `"10.0.0.0/8"` |  |
| hydra.hydra.config.serve.tls.allow_termination_from[1] | string | `"172.16.0.0/12"` |  |
| hydra.hydra.config.serve.tls.allow_termination_from[2] | string | `"172.20.0.0/16"` |  |
| hydra.hydra.config.serve.tls.allow_termination_from[3] | string | `"192.168.0.0/16"` |  |
| hydra.hydra.config.serve.tls.allow_termination_from[4] | string | `"127.0.0.1/32"` |  |
| hydra.hydra.config.strategies.access_token | string | `"jwt"` |  |
| hydra.hydra.config.ttl.access_token | string | `"1h"` |  |
| hydra.hydra.config.urls.consent | string | `"https://auth.auth-test.pleio-test.nl/consent"` |  |
| hydra.hydra.config.urls.login | string | `"https://auth.auth-test.pleio-test.nl/login"` |  |
| hydra.hydra.config.urls.logout | string | `"https://auth.auth-test.pleio-test.nl/logout"` |  |
| hydra.hydra.config.urls.self.issuer | string | `"https://oauth.auth-test.pleio-test.nl"` |  |
| hydra.hydra.config.urls.self.public | string | `"https://oauth.auth-test.pleio-test.nl"` |  |
| hydra.ingress.admin.enabled | bool | `false` |  |
| hydra.maester.enabled | bool | `false` |  |
| hydra.secret.enabled | bool | `false` |  |
| hydraAdminUrl | string | `"http://hydra-admin"` | Set this to ORY Hydra's Admin URL |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.repository | string | `"registry.gitlab.com/pleio/kratos-idm"` |  |
| image.tag | string | `"latest"` |  |
| imagePullSecrets | list | `[]` |  |
| ingress.annotations | object | `{}` |  |
| ingress.className | string | `""` |  |
| ingress.enabled | bool | `false` |  |
| ingress.hosts[0].host | string | `"chart-example.local"` |  |
| ingress.hosts[0].paths[0].path | string | `"/"` |  |
| ingress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` |  |
| ingress.tls | list | `[]` |  |
| kratos.fullnameOverride | string | `"idm-kratos"` |  |
| kratos.ingress.admin.enabled | bool | `false` |  |
| kratos.kratos.automigration.enabled | bool | `true` |  |
| kratos.kratos.config.cookies.domain | string | `"auth-test.pleio-test.nl"` |  |
| kratos.kratos.config.identity.default_schema_id | string | `"employee"` |  |
| kratos.kratos.config.identity.schemas[0].id | string | `"employee"` |  |
| kratos.kratos.config.identity.schemas[0].url | string | `"file:///etc/config/identity.employee.schema.json"` |  |
| kratos.kratos.config.serve.admin.base_url | string | `"http://idm-kratos-admin/"` |  |
| kratos.kratos.config.serve.public.base_url | string | `"https://idm.auth-test.pleio-test.nl/"` |  |
| kratos.kratos.identitySchemas."identity.employee.schema.json" | string | `"{\n  \"$id\": \"pleio.nl/schemas/employee\",\n  \"$schema\": \"http://json-schema.org/draft-07/schema#\",\n  \"title\": \"Employee\",\n  \"type\": \"object\",\n  \"properties\": {\n    \"traits\": {\n      \"type\": \"object\",\n      \"properties\": {\n        \"email\": {\n          \"type\": \"string\",\n          \"format\": \"email\",\n          \"title\": \"E-Mail\",\n          \"ory.sh/kratos\": {\n            \"credentials\": {\n              \"password\": {\n                \"identifier\": true\n              }\n            },\n            \"recovery\": {\n              \"via\": \"email\"\n            },\n            \"verification\": {\n              \"via\": \"email\"\n            }\n          }\n        },\n        \"name\": {\n          \"type\": \"object\",\n          \"properties\": {\n            \"first\": {\n              \"type\": \"string\",\n              \"description\": \"First name\",\n              \"title\": \"First name\"\n            },\n            \"last\": {\n              \"type\": \"string\",\n              \"description\": \"First name\",\n              \"title\": \"Last name\"\n            }\n          }\n        }\n      },\n      \"required\": [\n        \"email\"\n      ],\n      \"additionalProperties\": false\n    }\n  }\n}\n"` |  |
| kratos.kratos.oauth2_provider.url | string | `"http://idm-hydra-admin:4445"` |  |
| kratos.kratos.selfservice.allowed_return_urls[0] | string | `"https://auth.auth-test.pleio-test.nl"` |  |
| kratos.kratos.selfservice.default_browser_return_url | string | `"https://auth.auth-test.pleio-test.nl/"` |  |
| kratos.kratos.selfservice.flows.error.ui_url | string | `"https://auth.auth-test.pleio-test.nl/error"` |  |
| kratos.kratos.selfservice.flows.login.after.password.hooks[0].hook | string | `"require_verified_address"` |  |
| kratos.kratos.selfservice.flows.login.ui_url | string | `"https://auth.auth-test.pleio-test.nl/login"` |  |
| kratos.kratos.selfservice.flows.logout.after.default_browser_return_url | string | `"https://auth.auth-test.pleio-test.nl/login"` |  |
| kratos.kratos.selfservice.flows.recovery.after.hooks[0].hook | string | `"revoke_active_sessions"` |  |
| kratos.kratos.selfservice.flows.recovery.enabled | bool | `true` |  |
| kratos.kratos.selfservice.flows.recovery.ui_url | string | `"https://auth.auth-test.pleio-test.nl/recovery"` |  |
| kratos.kratos.selfservice.flows.registration.after.password.hooks[0].hook | string | `"session"` |  |
| kratos.kratos.selfservice.flows.registration.after.password.hooks[1].hook | string | `"show_verification_ui"` |  |
| kratos.kratos.selfservice.flows.registration.after.password.hooks[2].config.body | string | `"base64://ZnVuY3Rpb24oY3R4KSB7CiAgdXNlcklkOiBjdHguaWRlbnRpdHkuaWQsCn0="` |  |
| kratos.kratos.selfservice.flows.registration.after.password.hooks[2].config.method | string | `"POST"` |  |
| kratos.kratos.selfservice.flows.registration.after.password.hooks[2].config.response.ignore | bool | `true` |  |
| kratos.kratos.selfservice.flows.registration.after.password.hooks[2].config.response.parse | bool | `false` |  |
| kratos.kratos.selfservice.flows.registration.after.password.hooks[2].config.url | string | `"https://auth.auth-test.pleio-test.nl/hooks/registration"` |  |
| kratos.kratos.selfservice.flows.registration.after.password.hooks[2].hook | string | `"web_hook"` |  |
| kratos.kratos.selfservice.flows.registration.enabled | bool | `false` |  |
| kratos.kratos.selfservice.flows.registration.ui_url | string | `"https://auth.auth-test.pleio-test.nl/registration"` |  |
| kratos.kratos.selfservice.flows.settings.privileged_session_max_age | string | `"1m"` |  |
| kratos.kratos.selfservice.flows.settings.required_aal | string | `"highest_available"` |  |
| kratos.kratos.selfservice.flows.settings.ui_url | string | `"https://auth.auth-test.pleio-test.nl/settings"` |  |
| kratos.kratos.selfservice.flows.verification.enabled | bool | `true` |  |
| kratos.kratos.selfservice.flows.verification.ui_url | string | `"https://auth.auth-test.pleio-test.nl/verification"` |  |
| kratos.kratos.selfservice.methods.link.enabled | bool | `true` |  |
| kratos.kratos.selfservice.methods.password.enabled | bool | `true` |  |
| kratos.kratos.selfservice.methods.totp.config.issuer | string | `"auth.auth-test.pleio-test.nl"` |  |
| kratos.kratos.selfservice.methods.totp.enabled | bool | `true` |  |
| kratos.kratos.session.whoami.required_aal | string | `"highest_available"` |  |
| kratos.secret.enabled | bool | `false` |  |
| kratosAdminUrl | string | `"http://kratos-admin"` | Set this to ORY Kratos's Admin URL |
| kratosBrowserUrl | string | `"http://kratos-browserui"` | Set this to ORY Kratos's public URL accessible from the outside world. |
| kratosPublicUrl | string | `"http://kratos-public"` | Set this to ORY Kratos's public URL |
| logLevel | string | `"info"` |  |
| nameOverride | string | `""` |  |
| podSecurityContext.fsGroup | int | `10000` |  |
| podSecurityContext.fsGroupChangePolicy | string | `"OnRootMismatch"` |  |
| podSecurityContext.runAsGroup | int | `10000` |  |
| podSecurityContext.runAsNonRoot | bool | `true` |  |
| podSecurityContext.runAsUser | int | `10000` |  |
| podSecurityContext.seccompProfile.type | string | `"RuntimeDefault"` |  |
| replicaCount | int | `1` | Number of replicas in deployment |
| revisionHistoryLimit | int | `5` | Number of revisions kept in history |
| secret.enabled | bool | `false` | switch to false to prevent creating the secret |
| secret.hashSumEnabled | bool | `true` | switch to false to prevent checksum annotations being maintained and propogated to the pods |
| secret.nameOverride | string | `""` | Provide custom name of existing secret, or custom name of secret to be created |
| secret.secretAnnotations | object | `{"helm.sh/hook":"pre-install, pre-upgrade","helm.sh/hook-delete-policy":"before-hook-creation","helm.sh/hook-weight":"0","helm.sh/resource-policy":"keep"}` | Annotations to be added to secret. Annotations are added only when secret is being created. Existing secret will not be modified. |
| securityContext.allowPrivilegeEscalation | bool | `false` |  |
| securityContext.capabilities.drop[0] | string | `"ALL"` |  |
| securityContext.privileged | bool | `false` |  |
| securityContext.readOnlyRootFilesystem | bool | `false` |  |
| securityContext.runAsGroup | int | `10000` |  |
| securityContext.runAsNonRoot | bool | `true` |  |
| securityContext.runAsUser | int | `10000` |  |
| securityContext.seLinuxOptions.level | string | `"s0:c123,c456"` |  |
| securityContext.seccompProfile.type | string | `"RuntimeDefault"` |  |
| service.loadBalancerIP | string | `""` | The load balancer IP |
| service.name | string | `"http"` | The service port name. Useful to set a custom service port name if it must follow a scheme (e.g. Istio) |
| service.nodePort | string | `""` |  |
| service.port | int | `80` |  |
| service.type | string | `"ClusterIP"` |  |
